package io.telary.featherbrief.service

import org.scalatest._
import org.scalatest.matchers.should.Matchers

class OpmlParserTest extends FlatSpec
  with Matchers {
  "OpmlFile" should "be successfully parsed" in {
    val filename = "src/test/resources/opml.xml"
    val email = "test@email.com"
    val parsedFeed = OpmlParser.parse(filename, email)
    parsedFeed.size shouldEqual(12)
    parsedFeed.head.createdBy shouldBe email
  }
}
