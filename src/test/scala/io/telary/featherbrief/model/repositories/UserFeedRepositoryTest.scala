package io.telary.featherbrief.model.repositories

import io.telary.featherbrief.model.Defaults
import io.telary.featherbrief.model.objects.{Indices, UserFeed, UserFeedField}
import org.scalatest._
import org.scalatest.matchers.should.Matchers

class UserFeedRepositoryTest extends FlatSpec
  with Matchers with RepositorySuite {

  "User feed repository" should "successfully update ES database" in {
    val esClient = RepositoryUtil.esClient()
    val userFeedRepository = new UserFeedRepository(Indices.USER_FEED_TEST)
    val userFeed1 = new UserFeed("1", "1", "1")
    val userFeed2 = new UserFeed("2", "2", "1")
    val userFeed3 = new UserFeed("3", "3", "1", "example")
    val userFeed4 = new UserFeed("1", "1", "2")
    val userFeed5 = new UserFeed("2", "2", "2")
    userFeedRepository.index(esClient, userFeed1)
    userFeedRepository.index(esClient, userFeed2)
    userFeedRepository.index(esClient, userFeed3)
    userFeedRepository.index(esClient, userFeed4)
    userFeedRepository.index(esClient, userFeed5)
    Thread.sleep(1000)
    val exists = userFeedRepository.exists(esClient, "1", "1")
    exists shouldBe true
    val notExists = userFeedRepository.exists(esClient, "3", "2")
    notExists shouldBe false
    val allUfis = userFeedRepository.getAll(esClient, "1")
    allUfis.size shouldBe 3
    allUfis.head("category") shouldBe Defaults.DEFAULT_CATEGORY
    allUfis(2)("category") shouldBe "example"
    userFeedRepository.unfollow(esClient, "1", "1")
    userFeedRepository.update(esClient, feedName="2", feedUrl="2", email="1", category="updated")
    Thread.sleep(1000)
    val followedFeeds = userFeedRepository.getAll(esClient, "1")
    followedFeeds.size shouldBe 2
    followedFeeds(1)("category") shouldBe "updated"
  }

  "User feed repository" should "let me find the user following a feed" in {
    val esClient = RepositoryUtil.esClient()
    val userFeedRepository = new UserFeedRepository(Indices.USER_FEED_TEST)
    val userFeed1 = new UserFeed("4", "4", "1", "example")
    val userFeed2 = new UserFeed("5", "5", "1")
    val userFeed3 = new UserFeed("4", "4", "2")
    userFeedRepository.index(esClient, userFeed1)
    userFeedRepository.index(esClient, userFeed2)
    userFeedRepository.index(esClient, userFeed3)
    Thread.sleep(1000)
    val users = userFeedRepository.getAllUserFollowing(esClient, "4")
    users.size shouldBe 2
    for(user <- users) {
      if (user._1 == "1") {
        user._2 shouldBe "example"
      }
      if (user._1 == "2") {
        user._2 shouldBe Defaults.DEFAULT_CATEGORY
      }
    }
    val users2 = userFeedRepository.getAllUserFollowing(esClient, "5")
    users2.size shouldBe 1
    users2.head._1 shouldBe "1"
    users2.head._2 shouldBe Defaults.DEFAULT_CATEGORY
  }
}
