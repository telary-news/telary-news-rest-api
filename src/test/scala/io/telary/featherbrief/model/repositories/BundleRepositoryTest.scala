package io.telary.featherbrief.model.repositories

import io.telary.featherbrief.model.objects.{Bundle, Feed, Indices}
import org.scalatest._
import org.scalatest.matchers.should.Matchers

class BundleRepositoryTest extends FlatSpec
  with Matchers with RepositorySuite {

  "Bundle actions" should "successfully update ES database" in {
    val esClient = RepositoryUtil.esClient()
    val bundleRepository = new BundleRepository(Indices.BUNDLE_TEST)

    val bundleName1 = "Test_bundle_1"
    val bundleName2 = "Test_bundle_2"

    val bundle1 = new Bundle(
      bundleName1,
      "fr",
      List(
        Feed("test #1", "", 0, 0, isInStandby = false, "fr", "", "", "", parsable = true),
        Feed("test #2", "", 0, 0, isInStandby = false, "fr", "", "", "", parsable = true)
      )
    )

    val bundle2 = new Bundle(
      bundleName2,
      "fr",
      List(
        Feed("test #1", "", 0, 0, isInStandby = false, "fr", "", "", "", parsable = true),
        Feed("test #2", "", 0, 0, isInStandby = false, "fr", "", "", "", parsable = true),
        Feed("test #3", "", 0, 0, isInStandby = false, "fr", "", "", "", parsable = true),
        Feed("test #4", "", 0, 0, isInStandby = false, "fr", "", "", "", parsable = true)
      )
    )

    bundleRepository.insert(esClient, bundle1)
    bundleRepository.insert(esClient, bundle2)
    Thread.sleep(1000)
    val bundles = bundleRepository.getAll(esClient)
    bundles.size shouldBe 2
    bundles.head.name shouldBe bundleName1
    bundles.last.name shouldBe bundleName2

    bundles.head.feeds.size shouldBe 2
    bundles.last.feeds.size shouldBe 4
  }
}
