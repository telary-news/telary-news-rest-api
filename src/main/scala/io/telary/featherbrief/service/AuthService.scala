package io.telary.featherbrief.service

import io.telary.featherbrief.model.{UserInfo, UserInfoFields}
import io.vertx.core.json.JsonObject
import pdi.jwt.{Jwt, JwtAlgorithm}
import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets
import java.security.{MessageDigest, PublicKey}
import java.security.cert.CertificateFactory
import java.util.Base64

object AuthService {
  def isValid(token : String, secret : String) : Boolean = {
    Jwt.isValid(token, convertToPubKey(secret), Seq(JwtAlgorithm.RS256))
  }

  def decode(token : String, secret : String) : UserInfo = {
    val userInfoAsString = Jwt.decode(token, convertToPubKey(secret), Seq(JwtAlgorithm.RS256)).get
    val userInfo = new JsonObject(userInfoAsString)
    val userInfoAsMap = userInfo.getMap
    UserInfo(
        userInfoAsMap.getOrDefault(UserInfoFields.NICKNAME, "").toString,
        userInfoAsMap.getOrDefault(UserInfoFields.NAME, "").toString,
        userInfoAsMap.getOrDefault(UserInfoFields.PICTURE, "").toString,
        userInfoAsMap.getOrDefault(UserInfoFields.UPDATED_AT, "").toString,
        userInfoAsMap.getOrDefault(UserInfoFields.EMAIL, "").toString,
        userInfoAsMap.getOrDefault(UserInfoFields.EMAIL_VERIFIED, "").toString,
        userInfoAsMap.getOrDefault(UserInfoFields.ISS, "").toString,
        userInfoAsMap.getOrDefault(UserInfoFields.SUB, "").toString,
        userInfoAsMap.getOrDefault(UserInfoFields.AUD, "").toString,
        userInfoAsMap.getOrDefault(UserInfoFields.IAT, "").toString,
        userInfoAsMap.getOrDefault(UserInfoFields.EXP, "").toString,
        userInfoAsMap.getOrDefault(UserInfoFields.NONCE, "").toString
    )
  }

  def convertToPubKey(secret : String) : PublicKey = {
    CertificateFactory
      .getInstance("X.509")
      .generateCertificate(new ByteArrayInputStream(secret.getBytes))
      .getPublicKey
  }

  def hashEmail(salt : String, email : String) : String = {
    val sha256 = MessageDigest.getInstance("SHA-256")
    sha256.update(salt.getBytes("UTF-8"))
    val hashedEmail = sha256.digest(email.getBytes(StandardCharsets.UTF_8));
    val enc = Base64.getEncoder
    enc.encodeToString(hashedEmail)
  }
}
