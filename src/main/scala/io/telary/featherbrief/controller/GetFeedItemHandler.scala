package io.telary.featherbrief.controller

import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteHandler, RouteParams, RouteResponse}
import io.telary.featherbrief.model.objects.Indices
import io.telary.featherbrief.model.repositories.UserFeedItemRepository
import io.telary.featherbrief.service.NodeClient
import io.vertx.ext.web.FileUpload
import io.vertx.ext.web.client.WebClient

object GetFeedItemHandler extends RouteHandler {
  /**
   * handle the route processing
   *
   * @param routeParams : path and body params
   * @return the response
   */
  override def process(routeParams: RouteParams,
                                 webClient: Option[WebClient],
                                 esWrapper: Option[EsClientWrapper],
                                 nodeClient: Option[NodeClient],
                                 uploadedFiles: Option[List[FileUpload]]): RouteResponse = {
    val email = getEmail(routeParams)
    val feedUrl = getFeed(routeParams)
    val saved = isSavedFilterPresent(routeParams)
    val size : Int = routeParams.param.getOrElse("size", "5").asInstanceOf[String].toInt
    val userFeedItemRepository = new UserFeedItemRepository(esWrapper.get.indices(Indices.USER_FEED_ITEM))
    val feedItems = userFeedItemRepository.getAllFeedItemForUser(esWrapper.get.esClient, email, feedUrl, saved, size)
    RouteResponse(200, list = feedItems, listName = "items")
  }
}
