package io.telary.featherbrief.controller.utils

import java.time.LocalDate
import java.util.Date

import io.vertx.core.json.JsonObject

class LogMessage(
                  val route : String,
                  val action : String,
                  val date : String,
                  val code : Int,
                  val duration : Int,
                  val errorMessage : String
                ) {
  def toJson: JsonObject = {
    new JsonObject()
      .put("date", date)
      .put("route", route)
      .put("action", action)
      .put("code", code)
      .put("duration", duration)
      .put("error", errorMessage)
  }
}
