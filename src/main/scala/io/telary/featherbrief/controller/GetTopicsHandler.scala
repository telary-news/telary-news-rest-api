package io.telary.featherbrief.controller

import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteHandler, RouteParams, RouteResponse}
import io.telary.featherbrief.model.repositories.TopicRepository
import io.telary.featherbrief.service.NodeClient
import io.vertx.ext.web.FileUpload
import io.vertx.ext.web.client.WebClient
import org.elasticsearch.client.RestHighLevelClient

object GetTopicsHandler extends RouteHandler {
  /**
   * handle the route processing
   *
   * @param routeParams : path and body params
   * @return the response
   */
  override def process(routeParams: RouteParams,
                                 webClient: Option[WebClient],
                                 esWrapper: Option[EsClientWrapper],
                                 nodeClient: Option[NodeClient],
                                 uploadedFiles: Option[List[FileUpload]]): RouteResponse = {
    val email = getEmail(routeParams)
    val topics = TopicRepository.getAllTopics(esWrapper.get.esClient, email)
    RouteResponse(200, listName = "topics", list = topics)
  }
}
