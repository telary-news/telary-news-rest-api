package io.telary.featherbrief.controller

import io.telary.featherbrief.controller.PostFeedHandler.followFeed
import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteHandler, RouteParams, RouteResponse}
import io.telary.featherbrief.listener.actions.FeedActions
import io.telary.featherbrief.model.Defaults
import io.telary.featherbrief.model.objects.Indices
import io.telary.featherbrief.model.repositories.{UserFeedItemRepository, UserFeedRepository}
import io.telary.featherbrief.service.NodeClient
import io.telary.featherbrief.service.es.Converter
import io.vertx.ext.web.FileUpload
import io.vertx.ext.web.client.WebClient

object PutFeedHandler extends RouteHandler with FeedActions {
  /**
   * handle the route processing
   *
   * @param routeParams : path and body params
   * @return the response
   */
  override def process(
                        routeParams: RouteParams,
                        webClient: Option[WebClient],
                        esWrapper: Option[EsClientWrapper],
                        nodeClient: Option[NodeClient],
                        uploadedFiles: Option[List[FileUpload]]
                      ): RouteResponse = {
    val feedUrl = routeParams.param.getOrElse("url", "").toString
    val feedName = routeParams.param.getOrElse("name", "").toString
    val newCategory = routeParams.param.getOrElse("new_category", Defaults.DEFAULT_CATEGORY).toString
    val oldCategory = routeParams.param.getOrElse("old_category", Defaults.DEFAULT_CATEGORY).toString
    val userEmail = getEmail(routeParams)
    val userFeedRepository = new UserFeedRepository(esWrapper.get.indices(Indices.USER_FEED))
    val userFeedItemRepository = new UserFeedItemRepository(esWrapper.get.indices(Indices.USER_FEED_ITEM))
    val userFeed = userFeedRepository.update(esWrapper.get.esClient, feedName, feedUrl, userEmail, newCategory)
    userFeedItemRepository.updateFeedItemsCategory(esWrapper.get.esClient, userEmail, feedUrl, oldCategory, newCategory)
    RouteResponse(200, json = Converter.toJsonObject(userFeed).encodePrettily())
  }
}
