package io.telary.featherbrief.model

case class Parameters(
                       jwtSecret : String,
                       esUsername : String,
                       esPassword : String,
                       esHost : String,
                       esPort : String,
                       userAgent : String,
                       serverPort : String,
                       auth0Url : String,
                       auth0ClientId : String,
                       auth0ClientSecret : String,
                       auth0Audience : String,
                       auth0GrantType : String,
                       nodeServerHost : String,
                       nodeServerPort : String,
                       salt : String
                     )
