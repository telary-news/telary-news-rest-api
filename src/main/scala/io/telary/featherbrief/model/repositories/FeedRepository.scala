package io.telary.featherbrief.model.repositories

import java.util
import java.util.Collections

import io.telary.featherbrief.model.objects.{Feed, FeedField}
import io.telary.featherbrief.service.es.Converter
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.search.{SearchRequest, SearchResponse}
import org.elasticsearch.client.{RequestOptions, RestHighLevelClient}
import org.elasticsearch.common.unit.TimeValue
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.{SearchHit, SearchHits}
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.elasticsearch.action.search.ClearScrollRequest
import org.elasticsearch.action.search.ClearScrollResponse
import org.elasticsearch.action.search.SearchScrollRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.common.unit.TimeValue
import org.elasticsearch.index.reindex.{BulkByScrollResponse, UpdateByQueryAction, UpdateByQueryRequest, UpdateByQueryRequestBuilder}
import org.elasticsearch.script.{Script, ScriptType}
import org.elasticsearch.search.Scroll
import org.elasticsearch.search.sort.SortOrder

import scala.collection.mutable.ArrayBuffer
import scala.collection.JavaConverters._
import scala.collection.mutable


class FeedRepository(index : String) {

  def parse(map : java.util.Map[String, AnyRef]) : Feed = {
    val url = map.getOrDefault(FeedField.URL, "").toString
    val name = map.getOrDefault(FeedField.NAME, "").toString
    val frequency : Int = map.getOrDefault(FeedField.PUBLICATION_FREQUENCY, "1").toString.toInt
    val isInStandby : Boolean = map.getOrDefault(FeedField.IS_IN_STANDBY, "false").toString.toBoolean
    val language : String = map.getOrDefault(FeedField.LANGUAGE, "").toString
    val description : String = map.getOrDefault(FeedField.DESCRIPTION, "").toString
    val favicon : String = map.getOrDefault(FeedField.FAVICON, "").toString
    val createdBy : String = map.getOrDefault(FeedField.CREATED_BY, "").toString
    val parsable : Boolean = map.getOrDefault(FeedField.PARSABLE, "true").toString.toBoolean
    val lastPubDate : Long = if (!map.containsKey(FeedField.LAST_PUBLICATION_DATE)) {
     0
    } else {
      if (map.get(FeedField.LAST_PUBLICATION_DATE).isInstanceOf[Int]) {
        map.get(FeedField.LAST_PUBLICATION_DATE).asInstanceOf[Int].toLong
      } else if (map.get(FeedField.LAST_PUBLICATION_DATE).isInstanceOf[Long]) {
        map.get(FeedField.LAST_PUBLICATION_DATE).asInstanceOf[Long]
      } else {
        0L
      }
    }
    Feed(name, url, frequency, lastPubDate, isInStandby, language, description, favicon, createdBy, parsable)
  }

  def create(esClient: RestHighLevelClient, feedUrl: String, feedName: String, language: String, description: String,
             publicationFrequency: Int, standbyFrequency: Option[Int], favicon: String, createdBy: String,
             parsable: Boolean) : Feed = {
    val indexRequest = new IndexRequest(index)
    val pubFreq = if (standbyFrequency.isDefined) {
      standbyFrequency.get
    } else {
      publicationFrequency
    }
    val feed = Feed(
      feedName, feedUrl, pubFreq, 0, standbyFrequency.isDefined, language, description, favicon, createdBy, parsable
    )
    val feedAsString = Converter.toJsonObject(feed).encodePrettily()
    indexRequest.source(feedAsString, XContentType.JSON)
    esClient.index(indexRequest, RequestOptions.DEFAULT)
    feed
  }

  def update(esClient: RestHighLevelClient, feedUrl : String, publicationFrequency : Int, lastPubDate : Long) : Boolean = {
    val request = new UpdateByQueryRequest(index)
    val javaMap = new util.HashMap[String, Object]()
    val lastPub : java.lang.Long = lastPubDate
    val pubFreq : java.lang.Integer = publicationFrequency
    javaMap.put(FeedField.PUBLICATION_FREQUENCY, pubFreq)//publicationFrequency.toLong
    javaMap.put(FeedField.LAST_PUBLICATION_DATE, lastPub)
    request.setQuery(QueryBuilders.matchQuery(FeedField.URL + ".keyword", feedUrl))
      .setMaxDocs(1)
      .setScript(new Script(ScriptType.INLINE,
        "painless",
        """
        |ctx._source.%s = params.%s;
        |ctx._source.%s = params.%s;
        |""".stripMargin.format(
          FeedField.PUBLICATION_FREQUENCY,
          FeedField.PUBLICATION_FREQUENCY,
          FeedField.LAST_PUBLICATION_DATE,
          FeedField.LAST_PUBLICATION_DATE),
        javaMap
      ))
    val bulkResponse = esClient.updateByQuery(request, RequestOptions.DEFAULT);
    true
  }

  def get(esClient : RestHighLevelClient, url : String) : Option[Feed] = {
    val request = new SearchRequest(index)
    val searchSourceBuilder : SearchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.query(QueryBuilders.termQuery(FeedField.URL + ".keyword", url))
    searchSourceBuilder.size(1)
    request.source(searchSourceBuilder)
    val searchResponse : SearchResponse = esClient.search(request, RequestOptions.DEFAULT);
    val hits = searchResponse.getHits.getHits
    if (hits.nonEmpty) {
      val hit : SearchHit = hits(0)
      Some(parse(hit.getSourceAsMap))
    } else {
      None
    }
  }

  def getAll(esClient : RestHighLevelClient) : List[mutable.Map[String, AnyRef]] = {
    val request = new SearchRequest(index)
    val searchSourceBuilder : SearchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.query(QueryBuilders.existsQuery(FeedField.DESCRIPTION))
    searchSourceBuilder.sort(FeedField.PUBLICATION_FREQUENCY, SortOrder.DESC)
    searchSourceBuilder.size(50)
    request.source(searchSourceBuilder)
    val searchResponse : SearchResponse = esClient.search(request, RequestOptions.DEFAULT);
    val hits = searchResponse.getHits.getHits
    hits.map(hit => {
      hit.getSourceAsMap.asScala
    }).toList
  }

  def getForFrequency(esClient : RestHighLevelClient, frequency : Int) : List[Feed] = {
    val request = new SearchRequest(index)
    val scroll = new Scroll(TimeValue.timeValueMinutes(1L))
    val searchSourceBuilder : SearchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.query(QueryBuilders.boolQuery()
      .should(QueryBuilders.matchQuery(FeedField.PUBLICATION_FREQUENCY, frequency))
      .should(QueryBuilders.matchQuery(FeedField.LAST_PUBLICATION_DATE, 0))
    )
    searchSourceBuilder.size(100)
    request.source(searchSourceBuilder)
    request.scroll(scroll)

    var response = esClient.search(request, RequestOptions.DEFAULT)
    var scrollId : String  = response.getScrollId
    var hits : Array[SearchHit] = response.getHits.getHits
    var feeds : ArrayBuffer[Feed] = ArrayBuffer()

    while(hits != null && hits.length > 0)
    {
      val feedsPart : Array[Feed] = hits.map { hit =>
        parse(hit.getSourceAsMap)
      }
      feeds ++= feedsPart
      val scrollRequest = new SearchScrollRequest(scrollId)
      scrollRequest.scroll(scroll)
      response = esClient.scroll(scrollRequest, RequestOptions.DEFAULT)
      scrollId = response.getScrollId
      hits = response.getHits.getHits
    }

    val clearScrollRequest = new ClearScrollRequest
    clearScrollRequest.addScrollId(scrollId)
    val clearScrollResponse = esClient.clearScroll(clearScrollRequest, RequestOptions.DEFAULT)
    val succeeded = clearScrollResponse.isSucceeded
    feeds.toList
  }
}
