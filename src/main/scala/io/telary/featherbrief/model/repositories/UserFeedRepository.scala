package io.telary.featherbrief.model.repositories

import io.telary.featherbrief.model.objects.{Feed, FeedField, UserFeed, UserFeedField}
import io.telary.featherbrief.service.es.Converter
import io.vertx.core.json.JsonObject
import org.elasticsearch.action.DocWriteResponse.Result
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.search.{ClearScrollRequest, SearchRequest, SearchResponse, SearchScrollRequest}
import org.elasticsearch.action.update.UpdateRequest
import org.elasticsearch.client.{RequestOptions, RestHighLevelClient}
import org.elasticsearch.common.unit.TimeValue
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.index.reindex.{BulkByScrollResponse, DeleteByQueryRequest, UpdateByQueryRequest}
import org.elasticsearch.script.{Script, ScriptType}
import org.elasticsearch.search.{Scroll, SearchHit}
import org.elasticsearch.search.builder.SearchSourceBuilder

import java.util.Collections
import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}

class UserFeedRepository(index : String) {

  def getAll(esClient : RestHighLevelClient, email : String) : List[mutable.Map[String, AnyRef]] = {
    val request = new SearchRequest(index)
    val searchSourceBuilder : SearchSourceBuilder = new SearchSourceBuilder()
      .query(QueryBuilders.matchQuery(UserFeedField.EMAIL + ".keyword", email))
      .size(250)
    request.source(searchSourceBuilder)
    val searchResponse : SearchResponse = esClient.search(request, RequestOptions.DEFAULT)
    val hits = searchResponse.getHits.getHits
    hits.toList.map { hit =>
      hit.getSourceAsMap.asScala
    }
  }

  def exists(esClient: RestHighLevelClient, feedUrl: String, email: String) : Boolean = {
    val request = new SearchRequest(index)
    val searchSourceBuilder : SearchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.query(QueryBuilders.boolQuery()
      .must(QueryBuilders.termQuery(UserFeedField.FEED_URL + ".keyword", feedUrl))
        .must(QueryBuilders.termQuery(UserFeedField.EMAIL + ".keyword", email))
    )
    searchSourceBuilder.size(1)
    request.source(searchSourceBuilder)
    val searchResponse : SearchResponse = esClient.search(request, RequestOptions.DEFAULT);
    val hits = searchResponse.getHits.getHits
    if (hits.nonEmpty) {
      true
    } else {
      false
    }
  }

  def update(esClient: RestHighLevelClient, feedName: String, feedUrl: String, email: String, category: String) : UserFeed = {
    val updateRequest = new UpdateByQueryRequest(index)
    updateRequest.setQuery(QueryBuilders.boolQuery()
      .must(QueryBuilders.termQuery(UserFeedField.FEED_URL + ".keyword", feedUrl))
      .must(QueryBuilders.termQuery(UserFeedField.EMAIL + ".keyword", email))
    )
    updateRequest.setScript(
      new Script(
        ScriptType.INLINE, "painless",
        "ctx._source.category='%s'".format(category),
        Collections.emptyMap())
    )
    val bulkResponse = esClient.updateByQuery(updateRequest, RequestOptions.DEFAULT);
    UserFeed(
      feedName,
      feedUrl,
      email,
      category
    )
  }

  def index(esClient: RestHighLevelClient, feedUser: UserFeed) : Boolean = {
    val indexRequest = new IndexRequest(index)
    indexRequest.source(Converter.toJsonObject(feedUser).encodePrettily(), XContentType.JSON)
    val response = esClient.index(indexRequest, RequestOptions.DEFAULT)
    response.getResult == Result.CREATED
  }

  def getAllUserFollowing(esClient: RestHighLevelClient, url: String): List[(String, String)] = {
    val request = new SearchRequest(index)
    val scroll = new Scroll(TimeValue.timeValueMinutes(1L))
    val searchSourceBuilder : SearchSourceBuilder = new SearchSourceBuilder()
      .query(QueryBuilders.matchQuery(UserFeedField.FEED_URL +".keyword", url))
      .size(250)
    request
      .source(searchSourceBuilder)
      .scroll(scroll)

    var response = esClient.search(request, RequestOptions.DEFAULT)
    var scrollId : String  = response.getScrollId
    var hits : Array[SearchHit] = response.getHits.getHits
    var emails : ListBuffer[(String, String)] = ListBuffer()

    while(hits != null && hits.length > 0)
    {
      val partEmails = hits.map { hit =>
        (hit.getSourceAsMap.get(UserFeedField.EMAIL).toString,
          hit.getSourceAsMap.get(UserFeedField.CATEGORY).toString)
      }
      emails ++= partEmails
      val scrollRequest = new SearchScrollRequest(scrollId)
      scrollRequest.scroll(scroll)
      response = esClient.scroll(scrollRequest, RequestOptions.DEFAULT)
      scrollId = response.getScrollId
      hits = response.getHits.getHits
    }

    val clearScrollRequest = new ClearScrollRequest
    clearScrollRequest.addScrollId(scrollId)
    val clearScrollResponse = esClient.clearScroll(clearScrollRequest, RequestOptions.DEFAULT)
    val succeeded = clearScrollResponse.isSucceeded
    emails.toList
  }

  def unfollow(esClient: RestHighLevelClient, email: String, feedUrl: String) : Unit = {
    val request = new DeleteByQueryRequest(index)
    val query = QueryBuilders.boolQuery()
      .must(QueryBuilders.matchQuery(UserFeedField.FEED_URL + ".keyword", feedUrl))
      .must(QueryBuilders.matchQuery(UserFeedField.EMAIL + ".keyword", email))
    request.setQuery(query)
    val response = esClient.deleteByQuery(request, RequestOptions.DEFAULT)
  }

}
