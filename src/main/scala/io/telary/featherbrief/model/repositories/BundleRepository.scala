package io.telary.featherbrief.model.repositories

import java.util

import io.telary.featherbrief.model.objects.{Bundle, BundleField, Feed, FeedField}
import io.telary.featherbrief.service.es.Converter
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.search.{ClearScrollRequest, SearchRequest, SearchScrollRequest}
import org.elasticsearch.client.{RequestOptions, RestHighLevelClient}
import org.elasticsearch.common.unit.TimeValue
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.{Scroll, SearchHit}
import org.elasticsearch.search.builder.SearchSourceBuilder

import scala.collection.mutable.ArrayBuffer

class BundleRepository(index: String)  {

  def insert(esClient : RestHighLevelClient, bundle : Bundle) : Bundle = {
    val bundleAsString = Converter.toJsonObject(bundle).encodePrettily()
    val indexRequest = new IndexRequest(index)
    indexRequest.source(bundleAsString, XContentType.JSON)
    esClient.index(indexRequest, RequestOptions.DEFAULT)
    bundle
  }

  def parse(map : java.util.Map[String, AnyRef]) : Bundle = {
    val name : String = map.getOrDefault(BundleField.NAME, "").toString
    val language : String = map.getOrDefault(BundleField.LANGUAGE, "").toString
    val feedRepository = new FeedRepository("")
    val feeds = map.getOrDefault(BundleField.FEEDS, List()).asInstanceOf[java.util.ArrayList[Object]].toArray().map(feed => {
      feedRepository.parse(feed.asInstanceOf[java.util.Map[String, AnyRef]])
    })
    Bundle(name, language, feeds.toList)
  }

  def getAll(esClient : RestHighLevelClient) : List[Bundle] = {
    val request = new SearchRequest(index)
    val scroll = new Scroll(TimeValue.timeValueMinutes(1L))
    val searchSourceBuilder : SearchSourceBuilder = new SearchSourceBuilder()
    searchSourceBuilder.query(QueryBuilders.matchAllQuery())
    searchSourceBuilder.size(100)
    request.source(searchSourceBuilder)
    request.scroll(scroll)

    var response = esClient.search(request, RequestOptions.DEFAULT)
    var scrollId : String  = response.getScrollId
    var hits : Array[SearchHit] = response.getHits.getHits
    var bundles : ArrayBuffer[Bundle] = ArrayBuffer()

    while(hits != null && hits.length > 0)
    {
      val bundle : Array[Bundle] = hits.map { hit =>
        parse(hit.getSourceAsMap)
      }
      bundles ++= bundle
      val scrollRequest = new SearchScrollRequest(scrollId)
      scrollRequest.scroll(scroll)
      response = esClient.scroll(scrollRequest, RequestOptions.DEFAULT)
      scrollId = response.getScrollId
      hits = response.getHits.getHits
    }

    val clearScrollRequest = new ClearScrollRequest
    clearScrollRequest.addScrollId(scrollId)
    val clearScrollResponse = esClient.clearScroll(clearScrollRequest, RequestOptions.DEFAULT)
    val succeeded = clearScrollResponse.isSucceeded
    bundles.toList
  }
}
