package io.telary.featherbrief.model.repositories

import io.telary.featherbrief.model.Defaults

import java.net.URLEncoder
import java.util.Collections
import io.telary.featherbrief.model.objects.{UserFeedItem, UserFeedItemField}
import io.telary.featherbrief.service.es.Converter
import org.elasticsearch.action.bulk.BulkRequest
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.search.{SearchRequest, SearchResponse}
import org.elasticsearch.client.{RequestOptions, RestHighLevelClient}
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.index.reindex.UpdateByQueryRequest
import org.elasticsearch.script.{Script, ScriptType}
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.elasticsearch.search.sort.SortOrder

import scala.collection.JavaConverters._
import scala.collection.mutable

class UserFeedItemRepository(val index : String) {
  private val TRUE = "true"
  private val FALSE = "false"

  def insertAll(esClient: RestHighLevelClient, userFeedItems : List[UserFeedItem]) : Boolean = {
    val request = new BulkRequest
    if (userFeedItems.nonEmpty) {
      userFeedItems.foreach { ufi =>
        request
          .add(new IndexRequest(index)
            .source(Converter.toJsonObject(ufi).encodePrettily(), XContentType.JSON))
        println("Adding a feeditem to the bulk")
      }
      val bulkResponse = esClient.bulk(request, RequestOptions.DEFAULT)
      println("Sending the bulk")
    }
    true
  }

  def getFeedItemForUser(esClient: RestHighLevelClient, email: String, url: String) : UserFeedItem = {
    val request = new SearchRequest(index)
    val query = QueryBuilders.boolQuery()
      .must(QueryBuilders.matchQuery(UserFeedItemField.EMAIL + ".keyword", email))
      .must(QueryBuilders.matchQuery(UserFeedItemField.ARTICLE_URL + ".keyword", url))
    val searchSourceBuilder : SearchSourceBuilder = new SearchSourceBuilder()
      .query(query)
      .size(1)
    request.source(searchSourceBuilder)
    val searchResponse : SearchResponse = esClient.search(request, RequestOptions.DEFAULT)
    val hits = searchResponse.getHits.getHits
    if (hits.nonEmpty) {
      parse(hits.head.getSourceAsMap)
    } else {
      parse(new java.util.HashMap[String, AnyRef]())
    }
  }

  def parse(map: mutable.Map[String, AnyRef]): UserFeedItem = {
    parse(map.asJava)
  }

  def parse(map: java.util.Map[String, AnyRef]): UserFeedItem = {
    UserFeedItem(
      map.getOrDefault(UserFeedItemField.EMAIL, "").toString,
      map.getOrDefault(UserFeedItemField.FEED_NAME, "").toString,
      map.getOrDefault(UserFeedItemField.FEED_URL, "").toString,
      map.getOrDefault(UserFeedItemField.ARTICLE_URL, "").toString,
      map.getOrDefault(UserFeedItemField.AUTHOR, "").toString,
      map.getOrDefault(UserFeedItemField.DESCRIPTION, "").toString,
      map.getOrDefault(UserFeedItemField.DATE, "0").toString.toLong,
      map.getOrDefault(UserFeedItemField.SUMMARY, "").toString,
      map.getOrDefault(UserFeedItemField.TITLE, "").toString,
      map.getOrDefault(UserFeedItemField.SCORE, "0").toString.toLong,
      map.getOrDefault(UserFeedItemField.SHOWN, "false").toString.toBoolean,
      map.getOrDefault(UserFeedItemField.READ, "false").toString.toBoolean,
      map.getOrDefault(UserFeedItemField.SAVED, "false").toString.toBoolean,
      map.getOrDefault(UserFeedItemField.UPVOTE, "false").toString.toBoolean,
      map.getOrDefault(UserFeedItemField.DOWNVOTE, "false").toString.toBoolean,
      map.getOrDefault(UserFeedItemField.FAVICON, "").toString,
      map.getOrDefault(UserFeedItemField.PARSABLE, "true").toString.toBoolean,
      map.getOrDefault(UserFeedItemField.CATEGORY, Defaults.DEFAULT_CATEGORY).toString
    )
  }

  def getAllFeedItemForUser(esClient: RestHighLevelClient, email: String, feedUrl: Option[String], saved: Boolean, size: Int)
  : List[mutable.Map[String, AnyRef]] = {
    val request = new SearchRequest(index)
    val query = if (feedUrl.isDefined) {
      QueryBuilders.boolQuery()
        .must(QueryBuilders.matchQuery(UserFeedItemField.EMAIL + ".keyword", email))
        .must(QueryBuilders.matchQuery(UserFeedItemField.FEED_URL + ".keyword", feedUrl.get))
    } else if (saved) {
      QueryBuilders.boolQuery()
        .must(QueryBuilders.matchQuery(UserFeedItemField.EMAIL + ".keyword", email))
        .must(QueryBuilders.matchQuery(UserFeedItemField.SAVED, true))
    } else {
      QueryBuilders.boolQuery()
        .must(QueryBuilders.matchQuery(UserFeedItemField.EMAIL + ".keyword", email))
        .must(QueryBuilders.matchQuery(UserFeedItemField.SHOWN, false))
        .must(QueryBuilders.matchQuery(UserFeedItemField.READ, false))
        .must(QueryBuilders.matchQuery(UserFeedItemField.SAVED, false))
    }
    val searchSourceBuilder : SearchSourceBuilder = new SearchSourceBuilder()
      .query(query)
      .sort(UserFeedItemField.SCORE, SortOrder.DESC)
      .size(size)
    request.source(searchSourceBuilder)
    val searchResponse : SearchResponse = esClient.search(request, RequestOptions.DEFAULT)
    val hits = searchResponse.getHits.getHits
    hits.toList.map { hit =>
      hit.getSourceAsMap.asScala
    }
  }

  def markAsShown(esClient: RestHighLevelClient, email: String, articlesUrl: List[String]): Boolean = {
    setFieldTo(esClient, email, articlesUrl, UserFeedItemField.SHOWN, TRUE)
  }

  def markAsRead(esClient: RestHighLevelClient, email: String, articlesUrl: List[String]): Boolean = {
    setFieldTo(esClient, email, articlesUrl, UserFeedItemField.READ, TRUE)
  }

  def markAsSaved(esClient: RestHighLevelClient, email: String, articlesUrl: List[String]): Boolean = {
    setFieldTo(esClient, email, articlesUrl, UserFeedItemField.SAVED, TRUE)
  }

  def markAsUpvoted(esClient: RestHighLevelClient, email: String, articlesUrl: List[String]): Boolean = {
    setFieldTo(esClient, email, articlesUrl, UserFeedItemField.UPVOTE, TRUE)
  }

  def markAsDownvoted(esClient: RestHighLevelClient, email: String, articlesUrl: List[String]): Boolean = {
    setFieldTo(esClient, email, articlesUrl, UserFeedItemField.DOWNVOTE, TRUE)
  }

  def unmarkAsShown(esClient: RestHighLevelClient, email: String, articlesUrl: List[String]): Boolean = {
    setFieldTo(esClient, email, articlesUrl, UserFeedItemField.SHOWN, FALSE)
  }

  def unmarkAsRead(esClient: RestHighLevelClient, email: String, articlesUrl: List[String]): Boolean = {
    setFieldTo(esClient, email, articlesUrl, UserFeedItemField.READ, FALSE)
  }

  def unmarkAsSaved(esClient: RestHighLevelClient, email: String, articlesUrl: List[String]): Boolean = {
    setFieldTo(esClient, email, articlesUrl, UserFeedItemField.SAVED, FALSE)
  }

  def unmarkAsUpvoted(esClient: RestHighLevelClient, email: String, articlesUrl: List[String]): Boolean = {
    setFieldTo(esClient, email, articlesUrl, UserFeedItemField.UPVOTE, FALSE)
  }

  def unmarkAsDownvoted(esClient: RestHighLevelClient, email: String, articlesUrl: List[String]): Boolean = {
    setFieldTo(esClient, email, articlesUrl, UserFeedItemField.DOWNVOTE, FALSE)
  }

  private def setFieldTo(esClient: RestHighLevelClient, email: String, articlesUrl: List[String], field: String, value: String) : Boolean = {
    val query = QueryBuilders.boolQuery()
      .must(QueryBuilders.matchQuery(UserFeedItemField.EMAIL, email))
    val queryMust = QueryBuilders.boolQuery()
    articlesUrl.foreach{ url =>
      queryMust.should(QueryBuilders.matchQuery(UserFeedItemField.ARTICLE_URL + ".keyword", url))
    }
    query.must(queryMust)
    val request = new UpdateByQueryRequest(index)
    request.setQuery(query)
      .setMaxDocs(100)
      .setScript(new Script(ScriptType.INLINE,
        "painless",
        """
          |ctx._source.%s = %s;
          |""".stripMargin.format(field, value),
        Collections.emptyMap()
      ))
    val bulkResponse = esClient.updateByQuery(request, RequestOptions.DEFAULT);
    true
  }

  def updateFeedItemsCategory(esClient: RestHighLevelClient, email: String, feedUrl: String, oldCategory: String, newCategory: String): Boolean = {
    val query = QueryBuilders.boolQuery()
      .must(QueryBuilders.matchQuery(UserFeedItemField.FEED_URL + ".keyword", feedUrl))
      .must(QueryBuilders.matchQuery(UserFeedItemField.CATEGORY + ".keyword", oldCategory))
      .must(QueryBuilders.matchQuery(UserFeedItemField.EMAIL + ".keyword", email))
    val request = new UpdateByQueryRequest(index)
    request.setQuery(query)
      .setMaxDocs(100)
      .setScript(new Script(ScriptType.INLINE,
        "painless",
        """
          |ctx._source.%s = '%s';
          |""".stripMargin.format(UserFeedItemField.CATEGORY, newCategory),
        Collections.emptyMap()
      ))
    val bulkResponse = esClient.updateByQuery(request, RequestOptions.DEFAULT);
    bulkResponse.getUpdated > 0
  }
}
