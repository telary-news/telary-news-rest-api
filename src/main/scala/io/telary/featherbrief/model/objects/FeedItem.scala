package io.telary.featherbrief.model.objects

case class FeedItem(
                     articleUrl : String,
                     feedName : String,
                     feedUrl : String,
                     author : String,
                     description : String,
                     date : Long,
                     summary : String,
                     title : String
                   )
object FeedItemField {
  val ARTICLE_URL = "articleUrl"
  val FEED_NAME = "feedName"
  val FEED_URL = "feedUrl"
  val AUTHOR = "author"
  val DESCRIPTION = "description"
  val DATE = "date"
  val SUMMARY = "summary"
  val TITLE = "title"
}