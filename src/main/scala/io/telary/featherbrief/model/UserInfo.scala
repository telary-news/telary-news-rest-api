package io.telary.featherbrief.model

case class UserInfo( nickname : String,
                     name : String,
                     picture : String,
                     updated_at : String,
                     email : String,
                     email_verified : String,
                     iss : String,
                     sub : String,
                     aud : String,
                     iat : String,
                     exp : String,
                     nonce : String
                   )
