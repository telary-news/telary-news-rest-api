# Telary news Rest API

[![pipeline status](https://gitlab.com/feather-brief/feather-brief-rest-api/badges/master/pipeline.svg)](https://gitlab.com/feather-brief/feather-brief-rest-api/-/commits/master)
[![coverage report](https://gitlab.com/feather-brief/feather-brief-rest-api/badges/master/coverage.svg)](https://gitlab.com/feather-brief/feather-brief-rest-api/-/commits/master)

## Description

Telary news lets you follow and read news using the rss feed technology.
The main goals were :

- Leave the "mailing list" GUI for other rss feed aggregator (see the Dave Winer quote below)
- Keep the editors' websites as the center element of a rss feed because categories are cool but the content quality is only due to the content creator of the website
- Offer a cool privacy by design rss feed aggregator
- For us rss feed aggregator are the way to leave social network and go back to more quality based approach of reading content.
- No AI or too complex to be monitored recommendation algorithm
- Embed an ad-free reader for news articles and blog posts

```
I didn't think the mailbox approach to news was right. Who cares how many unread items there are. I like the river of news approach and I have a very fine set of rivers that keep me well supplied with news and podcasts.
- Dave Winer
```

## Technical description

We decided to build Telary news around some key concepts :

- **Privacy by design** : All stored emails are hashed and salted in the database so we can't use them to send you emails or stuff like that.
- **OAuth handles the security** : Because security and password are hard to manage we decided that OAuth would do it for us. They will probably do it far better than us as it is a well known actor.
- **Automate everything** : We like the gitlab-ci because we don't want to spent some time doing things that we will have to do multiple times.
- **Scala & some FP** : I like Java and the JVM but scala does feel like java on steroid.
- **Elasticsearch** : I really like to work with Elasticsearch and it really fits our usecase.
- **Kibana** : Because we're using ES, kibana is very good for our dashboards and stuff like that.
- **Vert.x** : I like the "express" like philosophy, all is explicit, all is fast and easy. The doc is also very good and it's not spring.
- **Logstash ready logs** : Our server logs are all in json and the listener is getting migrated to json. ``{"date":"2020-11-1711:45:16.728","route":"/secured/feeds","action":"[GET]","code":200,"duration":11,"error":""}``
- **PM2** : We use PM2 to run the 2 parts of the server.

## Contains

* __Server__ : Rest API serving data for the PWA
* __Listener__ : Follows RSS feed and store data in ES

## How to debug

* Server :
    * ``-s -c src/main/resources/CONFIG -p src/main/resources/PUB_KEY -d -e debug@email.com``
* Listener :
    * ``-l -c src/main/resources/CONFIG -p src/main/resources/PUB_KEY -d -e debug@email.com``

## How to build

````shell script
mvn clean install
````

## Kibana dashboard

![Dashboard sample](./readme_img/dashboard.png)

The dashboard displays useful data to monitor the system it shows :

- the total count of stored articles
- the total count of users
- the total amount of blogs followed
- the daily amount of connections
- the duplicate articles inserted (legacy because we had a duplication problem once)
- and the daily count of publications
